package com.quiz6;

public class Main {

    public static void main(String[] args) {
	Tree tree = new Tree();

        tree.insert(22);
        tree.insert(32);
        tree.insert(45);
        tree.insert(29);
        tree.insert(11);
        tree.insert(-4);
        tree.insert(79);
        tree.insert(9);

        tree.traverseInOrderDescending();
    }
}
