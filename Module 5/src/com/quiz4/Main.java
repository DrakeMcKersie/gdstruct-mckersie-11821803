package com.quiz4;

public class Main {

    public static void main(String[] args) {

        Player bog = new Player(11, "bog", 100);
        Player cog = new Player(12, "cog", 101);
        Player dog = new Player(13, "dog", 102);
        Player hog = new Player(14, "hog", 103);
        Player pog = new Player(15, "pog", 104);

        SimpleHashTable hashTable = new SimpleHashTable();
        hashTable.put(bog.getName(),bog);
        hashTable.put(cog.getName(),cog);
        hashTable.put(dog.getName(),dog);
        hashTable.put(hog.getName(),hog);
        hashTable.put(pog.getName(),pog);

       // hashTable.printHashtable();
        System.out.println(hashTable.removeKey("bog"));
    }

}
