package com.quiz2;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        Player calvin = new Player (1, "Calvin", 52);
        Player gunguy = new Player(2, "GunGuy25", 106);
        Player popoman = new Player(3, "P0p0Man", 268);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addtoFront(calvin);
        playerLinkedList.addtoFront(gunguy);
        playerLinkedList.addtoFront(popoman);

        playerLinkedList.remove();

        System.out.println(playerLinkedList.contains(new Player(1, "Calvin", 52)));
        System.out.println(playerLinkedList.indexOf(new Player(1, "Calvin", 52)));

        playerLinkedList.getSize(0);


        playerLinkedList.printList();

    }



}

