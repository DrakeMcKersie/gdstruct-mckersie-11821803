package com.quiz2;

public class PlayerLinkedList {
    private PlayerNode head;

    public void  addtoFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    public void printList(){
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null) {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public int getSize (PlayerNode node) {
        if (node == null) {
            return 0;
        }

        return getSize (node.next) + 1;
    }






}
